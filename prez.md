---
marp: true
paginate: true
_paginate: false
footer: M1MISO MASB 2021 - Université de Lille
size: 16:9
style: |
    li, p {
        font-size: .7em;
    }

    .flex {
        display: flex;
    }

    .box {
        flex: 1;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .box-wide {
        flex: 3;
    }
---

# biotools-rs
#### Biobiothèque rust pour le filtrage et l'assemblage de reads

[https://gitlab.com/protocod/biotools-rust](https://gitlab.com/protocod/biotools-rust)
[![](https://img.shields.io/badge/documentations-biotools--rs-blue)](https://protocod.gitlab.io/biotools-rust/biotools/)
Maximilien DI DIO

---

## Réalisation

- Filtrage de reads
- Assemblage de reads
- Parseur de fasta
- Intégration d'une interface en ligne de commande
- Intégration d'un parseur de fastq et de toml
- Approche en Test Driven Development
- Tests et de déploiement de documentations via Gitlab CI/CD

---

## Rust ?

<div class="flex">

<div class="box">

![rust.png](./assets/rust.png)

</div>

<div class="box-wide">
Rust est un langage de programmation compilé multi-paradigme conçu et développé par Mozilla Research depuis 2010. Il a été conçu pour être « un langage fiable, concurrent, pratique », supportant les styles de programmation purement fonctionnel, modèle d'acteur, procédural, ainsi qu'orienté objet sous certains aspects

<quote>fr.wikipedia.org</quote>
</div>

</div>

<center>

![ferris.gif](./assets/ferris.gif)
I am Ferris the crab, unofficial mascot for Rust

</center>

---

## Compilation

Assurer vous d'avoir installé rust sur votre machine. Sinon vous pouvez l'installer en utilisant rustup via la commande ci-dessous:

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

La compilation se fait avec cargo.
```sh
cargo build
```

Vous pouvez ajouter l'option `--release` pour générer un fichier exécutable optimisé et sans informations de débogages.

L'exécution peut se faire également avec cargo
```sh
cargo run -- --help
```

Tout ce qui suit après `--` sont des options pour le programme.

---

```sh
biotools 0.1.0
protocod/prx0 <contact.maximilien.didio@gmail.com>
Provide bioinformatics stuff

USAGE:
    biotools_main [FLAGS] [OPTIONS]

FLAGS:
    -a, --assembly            Used to make viral genome assembly from detected viral reads
    -f, --find-viral-reads    Used to filter reads
    -h, --help                Prints help information
    -V, --version             Prints version information
    -v, --verbose             Used to print lot of informations into stdout

OPTIONS:
    -c, --config <FILTER_CONFIG_FILE>    Sets a config file in toml for DNA/RNA filter
    -g, --genome <GENOME_FASTA_FILE>     Sets a genome in fasta file
    -r, --reads <READS_FASTQ_FILES>      Sets reads fastq files
```

**Exemple:**
Commande pour filtrer les reads suceptibles de contenir du génome virale.

```sh
cargo run -- --config config.toml --genome data/sars_cov2.fasta --reads data/reads-1M.fastq --find-viral-reads --verbose
```

---

## Exécution des tests unitaires

Commande pour exécuter les tests unitaires et récupérer des informations sur la couverture du code testé.
```
cargo tarpaulin --lib -v
```

ou alors cette commande pour ne pas récupérer d'informations sur la couverture du code testé.

```
cargo test --lib -v
```

---

## Structure de données utilisées

Le filtrage et l'assemblage de reads utilisent des tables de hashages qui associe à un nombre d'occurence pour chaque kmer.

Le nombre d'occurence d'un kmer dans une séquence est utile lors de l'extension d'un mot en vu de reconstuer une séquence par la formation de contigs.

![](./assets/genome-assembly.png)

L'implémentation choisie est [IndexMap](https://docs.rs/indexmap/1.6.2/indexmap/) qui est proche de la structure [collections::HashMap](https://doc.rust-lang.org/std/collections/struct.HashMap.html) issue de la biobliothèque standard de rust. À la différence que [IndexMap](https://docs.rs/indexmap/1.6.2/indexmap/) préserve l'ordre d'insertion des clés, cela permet de retrouver avec certitude le premier kmer inséré dans l'index constituant le mot à étendre.

---

## Difficultés rencontrés

**Techniques:**
- Nouveau langage de programmation, nouveaux concepts (Ownership ect)
- Organisation du code en une biobliothèque réutilisable

**Bioinformatiques**:
- Ajustement des paramètres sur de vrais données
- Compréhension de la méthode pour filtrer et assembler
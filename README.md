# biotools-rs

Biobiothèque rust pour le filtrage et l'assemblage de reads.

* [Compilation et exécution](#build-and-execute")
* [Tests unitaires](#unit-tests)
* [Usage](#usage")
* [Structure de données utilisées](#data-structures)

<div id="build-and-execute"></div>

## Compilation et exécution

Assurer vous d'avoir installé rust sur votre machine. Sinon vous pouvez l'installer en utilisant rustup via la commande ci-dessous:

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

La compilation se fait avec cargo.
```sh
cargo build
```

Vous pouvez ajouter l'option `--release` pour générer un fichier exécutable optimisé et sans informations de débogages.

L'exécution peut se faire également avec cargo
```sh
cargo run -- --help
```

Tout ce qui suit après `--` sont des options pour le programme. 


<div id="unit-tests"></div>

## Exécution des tests unitaires

Commande pour exécuter les tests unitaires et récupérer des informations sur la couverture du code testé.
```
cargo tarpaulin --lib -v
```


<div id="usage"></div>

## Usage

```
biotools 0.1.0
protocod/prx0 <contact.maximilien.didio@gmail.com>
Provide bioinformatics stuff

USAGE:
    biotools_main [FLAGS] [OPTIONS]

FLAGS:
    -a, --assembly            Used to make viral genome assembly from detected viral reads
    -f, --find-viral-reads    Used to filter reads
    -h, --help                Prints help information
    -V, --version             Prints version information
    -v, --verbose             Used to print lot of informations into stdout

OPTIONS:
    -c, --config <FILTER_CONFIG_FILE>    Sets a config file in toml for DNA/RNA filter
    -g, --genome <GENOME_FASTA_FILE>     Sets a genome in fasta file
    -r, --reads <READS_FASTQ_FILES>      Sets reads fastq files
```

Exemple:

Commande pour filtrer les reads suceptibles de contenir du génome virale.
```
cargo run -- --config config.toml --genome data/sars_cov2.fasta --reads data/reads-1M.fastq --find-viral-reads --verbose
```

Fichier de configuration associé.
```
kmer_size = 10
significance_ratio = 0.01
nthreads = 8
viral_reads_output_fastq = "data/viral_reads.fastq"
genome_assembly_output_fasta = "data/genome_assembly.fasta"
```

<div id="data-structures"></div>

## Structure de données utilisées

Le filtrage et l'assemblage de reads utilisent des tables de hashages qui associe à un nombre d'occurence pour chaque kmer.

Le nombre d'occurence d'un kmer dans une séquence est utile lors de l'extension d'un mot en vu de reconstuer une séquence par la formation de contigs.

![](./assets/genome-assembly.png)

L'implémentation choisie est [IndexMap](https://docs.rs/indexmap/1.6.2/indexmap/) qui est proche de la structure [collections::HashMap](https://doc.rust-lang.org/std/collections/struct.HashMap.html) issue de la biobliothèque standard de rust. À la différence que [IndexMap](https://docs.rs/indexmap/1.6.2/indexmap/) préserve l'ordre d'insertion des clés, cela permet de retrouver avec certitude le premier kmer inséré dans l'index constituant le mot à étendre.
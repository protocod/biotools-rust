use clap::App;


pub struct Args {
    pub reads_filepath: String,
    pub genome_filepath: String,
    pub config_filepath: String,
    pub assembly: bool,
    pub verbose: bool,
    pub find_viral_reads: bool
}


pub fn args() -> Args {
    let yaml = clap::load_yaml!("../cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    let verbose = matches.is_present("verbose");
    let assembly = matches.is_present("assembly");
    let filter = matches.is_present("find-viral-reads");

    let config_filepath = matches.value_of("config").unwrap_or("config.toml");
    let genome_filepath = matches.value_of("genome").unwrap();
    let reads_filepath = matches.value_of("reads").unwrap();

    Args {
        verbose: verbose,
        assembly: assembly,
        find_viral_reads: filter,
        config_filepath: config_filepath.to_owned(),
        genome_filepath: genome_filepath.to_owned(),
        reads_filepath: reads_filepath.to_owned()
    }
}
use serde_derive::{Deserialize};


#[derive(Deserialize, Debug, PartialEq, Clone)]
pub struct FilterConfig {
    pub kmer_size: usize,
    pub significance_ratio: f64,
    pub nthreads: usize,
    pub viral_reads_output_fastq: String,
    pub genome_assembly_output_fasta: String
}
pub mod model;

use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::path::Path;
use toml;

pub use crate::cli::Args;
pub use crate::io;
pub use crate::fasta;
pub use crate::genomic;

use fastq::{parse_path, Record};
use indexmap::{IndexMap, indexmap};


#[derive(Debug, Clone)]
pub struct Filter {
    pub reads_filepath: String,
    pub config: model::FilterConfig,
    pub index: IndexMap<Vec<u8>, usize>,
    pub index_rev: IndexMap<Vec<u8>, usize>,
    pub verbose: bool
}


impl Filter {
    pub fn new(reads_filepath: String, genome: Vec<u8>, config: model::FilterConfig, verbose: bool) -> Filter {
        let index = match get_kmer_index(&genome, config.kmer_size) {
            Ok(index) => index.clone(),
            Err(e) => panic!(e)
        };

        let reversed_genome: Vec<u8> = genome.iter().rev().map(|x| x.to_owned()).collect();

        let index_rev = match get_kmer_index(&reversed_genome, config.kmer_size) {
            Ok(index) => index.clone(),
            Err(e) => panic!(e)
        };

        Filter {
            reads_filepath,
            config,
            index,
            index_rev,
            verbose
        }
    }


    pub fn from_args(args: &Args) -> Filter {
        let genome_fasta_result = match File::open(Path::new(&args.genome_filepath)) {
            Ok(mut f) => {
                let mut contents = String::new();
                f.read_to_string(&mut contents).expect("Unable to read fasta content");
                fasta::parser::parse(contents.as_ref())
            }
            Err(e) => {
                panic!("{}", e);
            }
        };

        let genome_fasta_vec = genome_fasta_result.expect("Unable to parse fasta");

        let mut config_file_reader = io::Reader::new(&args.config_filepath);
        let config:model::FilterConfig = toml::from_str(config_file_reader.get_content_file().as_str())
            .expect("Unable to parse configuration file content");

        Filter::new(
            args.reads_filepath.clone(),
            genome_fasta_vec.get(0).unwrap().seq.clone(),
            config,
            args.verbose
        )
    }


    pub fn is_viral_read(&self, read: &impl Record) -> bool {
        let is_seq_viral = match is_viral_seq(
            &self.index,
            read.seq(),
            self.config.kmer_size,
            self.config.significance_ratio,
            self.verbose
        ) {
            Ok(b) => b,
            Err(e) => panic!(e),
        };

        let reversed: genomic::DNA = read.seq().iter().rev().map(|n| n.to_owned()).collect();
        let reverse_complement: &[u8] = &genomic::complementary(&reversed);

        let is_reverse_complement_viral = match is_viral_seq(
            &self.index_rev,
            reverse_complement,
            self.config.kmer_size,
            self.config.significance_ratio,
            self.verbose
        ) {
            Ok(b) => b,
            Err(e) => panic!(e),
        };

        is_seq_viral || is_reverse_complement_viral
    }


    fn write_read_into_file(&self, read: &impl Record) {
        let is_viral_read = self.is_viral_read(read);

        if !is_viral_read {
            return;
        }

        let filepath: String = self.config.viral_reads_output_fastq.clone();

        if !std::path::Path::new(&filepath).exists() {
            let _ = File::create(&filepath);
        }

        let mut file = OpenOptions::new()
            .append(true)
            .open(&filepath)
            .expect("Unable to create or open file");

        let fastq_seq = [
            read.head(),
            "\n".as_bytes(),
            read.seq(),
            "\n+\n".as_bytes(),
            read.qual(),
            "\n".as_bytes()
        ];

        for row in &fastq_seq {
            let _ = file.write(row);
        }
    }


    pub fn build_filtered_reads(&self) {
        let this = self.clone();        
        parse_path(Some(self.reads_filepath.clone()), move |parser| {
            let results: Vec<usize> = parser.parallel_each(this.config.nthreads, move |record_sets| {
                let mut thread_total = 0;
    
                for record_set in record_sets {
                    for record in record_set.iter() {
                        this.write_read_into_file(&record);
                        thread_total += 1;
                    }
                }

                thread_total
            }).expect("Invalid fastq file");
            println!("{} reads", results.iter().sum::<usize>());
        }).expect("Invalid compression");
    }


    pub fn genome_assembly(&self) -> Vec<u8> {
        let sequence = get_sequence_from_fastq(self.config.viral_reads_output_fastq.clone());
        genome_assembly(&sequence, self.config.kmer_size)
    }
}


pub fn get_kmer_index(genome_ref: &Vec<u8>, kmer_size: usize) -> Result<IndexMap<Vec<u8>, usize>, String> {
    if genome_ref.len() < kmer_size {
        return Err(format!("kmer_size too big, genome size {}", genome_ref.len()));
    }

    let end_loop = genome_ref.len() - kmer_size + 1;
    let mut kmer_hashset: IndexMap<Vec<u8>, usize> = IndexMap::new();

    let range =  0..end_loop;

    for n in range {
        let e = n+kmer_size;
        let kmer = &genome_ref[n..e];
        
        if let Some(&occurences) = &mut kmer_hashset.get(&kmer.to_vec()) {
            kmer_hashset.insert(kmer.to_vec(), occurences+1usize);
        } else {
            kmer_hashset.insert(kmer.to_vec(), 1);
        }
    }

    Ok(kmer_hashset)
}


pub fn is_viral_seq(
    index: &IndexMap<Vec<u8>, usize>,
    seq: &[u8],
    kmer_size: usize,
    significance_ratio: f64,
    verbose: bool) -> Result<bool, String> {
    if seq.len() < kmer_size {
        return Err(format!("kmer_size too big, genome size {}", seq.len()));
    }
    
    let end_loop = seq.len() - kmer_size + 1;
    let mut kmer_found_counter = 0usize;
    let mut kmers_counter = 0usize; 

    for n in 0..end_loop {
        kmers_counter += 1;
        let e = n + kmer_size;
        let word: Vec<u8> = seq[n..e].to_vec();
        if let Some(_) = index.get(&word) {
            kmer_found_counter += 1;
        }
    }

    let kmer_found_ratio = kmer_found_counter as f64 / kmers_counter as f64;
    log(format!("kmer_found_counter: {}, kmers_counter: {}", &kmer_found_counter, &kmers_counter), verbose);

    Ok(kmer_found_ratio > significance_ratio)
}


pub fn get_sequence_from_fastq(path: String) -> Vec<u8> {
    let mut sequence_from_reads: Vec<u8> = vec![];

    let _ = parse_path(Some(path.clone()), |parser| {
        let _  = parser.each(|record| {
            let seq = record.seq();
            sequence_from_reads.extend_from_slice(seq);
            record.validate_dnan()
        }).expect("Invalid fastq file");
    });

    sequence_from_reads
}


pub fn extend_word(word: &Vec<u8>, index: &IndexMap<Vec<u8>, usize>, mut built_sequence: Vec<u8>) -> Vec<u8>{
    let sliced_word = &word[1..].to_vec();

    let nucleotides = vec![
        "A".as_bytes()[0], 
        "T".as_bytes()[0],
        "C".as_bytes()[0],
        "G".as_bytes()[0]
    ];

    let available_nucleotides: Vec<&u8> = nucleotides.iter().filter(|n: &&u8| -> bool {
        let nucleotide = n.to_owned().to_owned();
        let mut word_to_find = sliced_word.clone();
        let _ = word_to_find.push(nucleotide);
        match index.get(&word_to_find) {
            Some(_) => true,
            _ => false
        }
    }).collect();

    println!("n found: {:?}, sliced_word size: {:?} sliced_word: {:?}", available_nucleotides, sliced_word.len(), std::str::from_utf8(sliced_word.as_slice()).unwrap());

    if available_nucleotides.len() > 0 {
        let recursive_call = |i: usize, available_nucleotides: &Vec<&u8>, sliced_word: &std::vec::Vec<u8>, word: &std::vec::Vec<u8>| {
            let mut new_index = index.clone();
            new_index.remove(word);
            let n = available_nucleotides.get(i).unwrap();
            built_sequence.push(n.to_owned().to_owned());
            let mut new_word = sliced_word.clone();
            new_word.push(n.to_owned().to_owned());
            extend_word(&new_word, &new_index, built_sequence)
        };


        if available_nucleotides.len() == 1 {
            return recursive_call(0, &available_nucleotides, sliced_word, word);
        } else {
            let occurences: Vec<&usize> = available_nucleotides.iter().map(|n| {
                let nucleotide = n.to_owned().to_owned();
                let mut word_to_find = sliced_word.clone();
                let _ = word_to_find.push(nucleotide);
                index.get(&word_to_find).unwrap()
            }).collect();

            let mut index_max = 0usize;

            for (index, &occurence) in occurences[1..].iter().enumerate() {
                if occurences[index_max] < occurence {
                    index_max = index;
                }
            }

            return recursive_call(index_max, &available_nucleotides, sliced_word, word);
        }
    }

    built_sequence
}


pub fn genome_assembly(sequence: &Vec<u8>, kmer_size: usize) -> Vec<u8> {
    let index = get_kmer_index(sequence, kmer_size).expect("Unable to create kmer index");
    let mut word = index.get_index(0).unwrap().0.to_owned();
    let mut extension = extend_word(&word, &index, vec![]);
    word.append(&mut extension);
    word.clone()
}


pub fn log(message: String, verbose: bool) {
    if verbose {
        println!("{}", message);
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    
    #[test]
    pub fn test_get_kmer_index() {
        struct Args {
            pub genome_ref: Vec<u8>,
            pub kmer_size: usize
        };
        struct TestingData {
            pub name: String,
            pub args: Args,
            pub expected: Result<IndexMap<Vec<u8>, usize>, String>
        };
        let tests = [
            TestingData {
                name: "stupid genome ref".to_owned(),
                args: Args {
                    genome_ref: "AAATGCC".as_bytes().to_vec(), // reverse: CCGTAAA
                    kmer_size: 3usize
                },
                expected: Ok(indexmap![
                        "AAA".as_bytes().to_vec() => 1,
                        "AAT".as_bytes().to_vec() => 1,
                        "ATG".as_bytes().to_vec() => 1,
                        "TGC".as_bytes().to_vec() => 1,
                        "GCC".as_bytes().to_vec() => 1,
                    ]
                    .into_iter()
                    .collect())
            },
            TestingData {
                name: "bad kmer size".to_owned(),
                args: Args {
                    genome_ref: "AA".as_bytes().to_vec(),
                    kmer_size: 3usize
                },
                expected: Err("kmer_size too big, genome size 2".to_owned())
            },
            TestingData {
                name: "stupid genome ref".to_owned(),
                args: Args {
                    genome_ref: "AAATGCCAAA".as_bytes().to_vec(), // reverse: CCGTAAA
                    kmer_size: 3usize
                },
                expected: Ok(indexmap![
                        "AAA".as_bytes().to_vec() => 2,
                        "AAT".as_bytes().to_vec() => 1,
                        "ATG".as_bytes().to_vec() => 1,
                        "TGC".as_bytes().to_vec() => 1,
                        "GCC".as_bytes().to_vec() => 1,
                        "CCA".as_bytes().to_vec() => 1,
                        "CAA".as_bytes().to_vec() => 1,
                    ]
                    .into_iter()
                    .collect())
            },
        ];
        for test in &tests {
            let Args{ genome_ref, kmer_size } = &test.args;
            let result = get_kmer_index(genome_ref, kmer_size.to_owned());
            assert_eq!(result, test.expected);
        }
    }
    
    
    #[test]
    pub fn test_is_viral_seq() {
        #[derive(Clone)]
        struct Args {
            index: IndexMap<Vec<u8>, usize>,
            sequences: Vec<Vec<u8>>,
            kmer_size: usize,
            significance_ratio: f64,
            verbose: bool
        };
        struct TestingData {
            pub name: String,
            pub args: Args,
            pub expected: Vec<bool>,
        };
        let tests = vec![
            TestingData {
                name: "2 reads 1 viral".to_owned(),
                args: Args {
                    index: get_kmer_index(&"AAAAAAAAAAAAAAAAAAAA".as_bytes().to_vec(), 10usize).unwrap(),
                    sequences: vec![
                        "AATTTTTACCGGTGTGAAAAAAAGGTTTAAAAAAAGTGAAAAAAAAAAAAAAAGTTTTTTAAAGGGGCCCCCCCCCGGGGGGGAAAAATTTTTTCCCCCCGGGCGCGCGACAGAGCTGGCAGGGGAAAACTTTTGCAGACAGAAAAAAAAAATTTTTTGGGGCCC".as_bytes().to_vec(),
                        "AAAAAAAAAAGGTGTGAAAAAAAGGAAAAAAAAAAGTGAAAAAAAAAAAAAAAGTTTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA".as_bytes().to_vec()
                    ],
                    kmer_size: 10usize,
                    significance_ratio: 0.5f64,
                    verbose: true
                },
                expected: vec![ false, true ]
            }
        ];
        for test in &tests {
            let Args{ 
                index,
                sequences,
                kmer_size,
                significance_ratio,
                verbose
            } = test.args.clone();
            let computed: Vec<bool>= sequences
                .into_iter()
                .map(|sequence| is_viral_seq(&index, &sequence, kmer_size, significance_ratio , verbose).unwrap())
                .collect();
            assert_eq!(test.expected, computed);
        }
    }
    
    
    #[test]
    pub fn test_config_parser() {
        let mut config_file_reader = io::Reader::new("config_test.toml");
        let parsed_config:model::FilterConfig = toml::from_str(config_file_reader.get_content_file().as_str())
            .expect("Unable to parse configuration file content");
        let expected_config =model::FilterConfig {
            kmer_size: 6usize,
            significance_ratio: 0.6f64,
            nthreads: 8usize,
            viral_reads_output_fastq: String::from("data/viral_reads.fastq"),
            genome_assembly_output_fasta: String::from("data/genome_assembly.fasta")
        };
        assert_eq!(parsed_config, expected_config);
    }


    #[test]
    pub fn test_genome_assembly() {
        struct Args {
            sequence: Vec<u8>, 
            kmer_size: usize
        };

        struct TestingData {
            name: String,
            args: Args,
            expected: Vec<u8>
        };


        let tests = vec![
            TestingData {
                name: "Using a small genome".to_owned(),
                args: Args {
                    sequence: "AGATACAGCCATGACCGTAGCATGCTAACTGTGACGGCATTAC".as_bytes().to_vec(),
                    kmer_size: 7usize
                },
                expected: "AGATACAGCCATGACCGTAGCATGCTAACTGTGACGGCATTAC".as_bytes().to_vec()
            }
        ];

        for test in &tests {
            let Args{ sequence, kmer_size } = &test.args;
            let computed = genome_assembly(sequence, kmer_size.to_owned());
            assert_eq!(test.expected, computed);
        }
    }
}
#[derive(Debug, PartialEq)]
pub struct Fasta {
    pub header: String,
    pub seq: Vec<u8>
}
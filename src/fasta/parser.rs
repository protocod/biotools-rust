use regex;
pub use crate::fasta::model;


pub fn parse<'a>(content: &str) -> Result<Vec<model::Fasta>, regex::Error> {
    let re = regex::Regex::new(r"(?P<header>>.*\n)(?P<seq>[ATCG]*)")?;
    let build_fasta_from_capture = |captured: regex::Captures| {
        let seq = captured.name("seq").unwrap().as_str().to_owned();
        model::Fasta { 
            header: captured.name("header").unwrap().as_str().to_owned(),
            seq: seq.as_bytes().to_vec()
        }
    };
    Ok(re.captures_iter(&content).map(build_fasta_from_capture).collect())
}


#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    pub fn parse_test() {
        struct TestingData {
            pub name: String,
            pub input: String,
            pub expected: Vec<model::Fasta>
        };
    
        let tests = [
            TestingData {
                name: "mono fasta".to_owned(),
                input: ">HAGV01071440.1 TSA: Squalus acanthias, contig TR91722|c0_g2_i1, transcribed RNA sequence\nGACCCCCATCAAGCTGGGAGTGTCCCCCACAGCAGCACAGAGATGGCGGCTCACAGACAGACAGCAGACA".to_owned(),
                expected: vec![
                    model::Fasta {
                        header: ">HAGV01071440.1 TSA: Squalus acanthias, contig TR91722|c0_g2_i1, transcribed RNA sequence\n".to_owned(),
                        seq: "GACCCCCATCAAGCTGGGAGTGTCCCCCACAGCAGCACAGAGATGGCGGCTCACAGACAGACAGCAGACA".as_bytes().to_vec()
                    }
                ]
            },
            TestingData {
                name: "multi fasta".to_owned(),
                input: ">HAGV01071440.1 TSA: Squalus acanthias, contig TR91722|c0_g2_i1, transcribed RNA sequence\nGACCCCCATCAAGCTGGGAGTGTCCCCCACAGCAGCACAGAGATGGCGGCTCACAGACAGACAGCAGACA\n>HAGV01071440.1 TSA: Squalus acanthias, contig TR91722|c0_g2_i1, transcribed RNA sequence\nGACCCCCATCAAGCTGGGAGTGTCCCCCACAGCAGCACAGAGATGGCGGCTCACAGACAGACAGCAGACA".to_owned(),
                expected: vec![
                    model::Fasta {
                        header: ">HAGV01071440.1 TSA: Squalus acanthias, contig TR91722|c0_g2_i1, transcribed RNA sequence\n".to_owned(),
                        seq: "GACCCCCATCAAGCTGGGAGTGTCCCCCACAGCAGCACAGAGATGGCGGCTCACAGACAGACAGCAGACA".as_bytes().to_vec()
                    },
                    model::Fasta {
                        header: ">HAGV01071440.1 TSA: Squalus acanthias, contig TR91722|c0_g2_i1, transcribed RNA sequence\n".to_owned(),
                        seq: "GACCCCCATCAAGCTGGGAGTGTCCCCCACAGCAGCACAGAGATGGCGGCTCACAGACAGACAGCAGACA".as_bytes().to_vec()
                    }
                ]
            }
        ];
    
        for test in &tests {
            let fasta_vec = parse(&test.input).unwrap();
            assert_eq!(true, fasta_vec == test.expected);
        }
    }
}
use indexmap::indexmap;


pub type DNA = Vec<u8>;


pub fn complementary(dna: &DNA) -> DNA {
    let rules = indexmap!{
        "A".as_bytes()[0] => "T".as_bytes()[0],
        "T".as_bytes()[0] => "A".as_bytes()[0],
        "C".as_bytes()[0] => "G".as_bytes()[0],
        "G".as_bytes()[0] => "C".as_bytes()[0]
    };

    dna.iter().map(|n| {
        match rules.get(n) {
            Some(v) => v.clone(),
            _ => "".as_bytes()[0]
        }
    }).collect()
}


#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    pub fn test_complementary() {
        struct Args {
            pub dna: DNA,
        };
        struct TestingData {
            pub name: String,
            pub args: Args,
            pub expected: DNA
        };

        let tests = vec![
            TestingData {
                name: "With a simple dna sequence".to_owned(),
                args: Args {
                    dna: "ATCG".as_bytes().to_vec()
                },
                expected: "TAGC".as_bytes().to_vec()
            }
        ];

        for test in &tests {
            let Args { dna } = &test.args;
            assert_eq!(test.expected, complementary(dna));
        }
    }
}
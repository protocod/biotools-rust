use std::fs::File;
use std::io::prelude::*;


pub struct Reader {
    pub path: String,
    file: File,
}


impl Reader {
    pub fn new(path: &str) -> Reader {
        let file = File::open(path).expect(&format!("file found not found at {}", path));
        Reader {
            path: path.to_owned(),
            file,
        }
    }

    pub fn get_content_file(&mut self) -> String {
        let mut buffer = String::new();
        self.file.read_to_string(&mut buffer).expect("Unable to get the Reader buffer as str");
        buffer.clone()
    }
}


#[cfg(test)]
mod tests {
    use super::*;


    #[test]
    pub fn test_reader() {
        let mut reader = Reader::new("data/reader_file_test.txt");
        let content_file = reader.get_content_file();
        assert_eq!("Hello world!", content_file);
    }
}
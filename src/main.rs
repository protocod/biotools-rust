pub use biotools::{ cli, filter };


fn main() {
    let args = cli::args();
    let filter = filter::Filter::from_args(&args);

    if args.find_viral_reads {
        filter.build_filtered_reads();
    }

    if args.assembly {
        filter.genome_assembly();
    }
}
